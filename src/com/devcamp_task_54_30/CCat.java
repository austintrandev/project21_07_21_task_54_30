package com.devcamp_task_54_30;

import com.cpet.CPet;

public class CCat extends CPet {
	public CCat(int age, String name) {
		super();
		this.age = age;
		this.name = name;
	}
	protected void bark() {
		System.out.println("Cat barking...");
	}

	protected void print() {
		System.out.println("Cat name: " + name);
		System.out.println("Cat age: " + this.age);
	}

	protected void playing() {
		System.out.println("Cat playing...");
	}
}
