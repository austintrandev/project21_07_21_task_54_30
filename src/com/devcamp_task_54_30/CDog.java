package com.devcamp_task_54_30;

import com.cpet.CPet;

public class CDog extends CPet {
	public CDog(int age, String name) {
		super();
		this.age = age;
		this.name = name;
	}

	protected void bark() {
		System.out.println("Dog barking...");
	}

	protected void print() {
		System.out.println("Dog name: " + name);
		System.out.println("Dog age: " + this.age);
	}

	protected void playing() {
		System.out.println("Dog playing...");
	}
}
