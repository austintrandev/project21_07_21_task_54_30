package com.devcamp_task_54_30;

import com.cpet.CPet;

public class MainPet {
	public static void main(String[] args) {
		CCat myPet = new CCat(8, "Sunny");
		//((CPet)myPet).print();
		System.out.println("------------");
		myPet.bark();
		myPet.print();
		myPet.playing();
		
		CDog myPet2 = new CDog(4,"Titi");
		System.out.println("------------");
		myPet2.bark();
		myPet2.print();
		myPet2.playing();
		
		CPet myPet3 = new CCat(4,"Momo");
		System.out.println("------------");
		((CCat)myPet3).bark();
		((CCat)myPet3).print();
		((CCat)myPet3).playing();
		
		myPet3 = new CDog(1,"Rain");
		System.out.println("------------");
		((CDog)myPet3).bark();
		((CDog)myPet3).print();
		((CDog)myPet3).playing();
	}
	
}
