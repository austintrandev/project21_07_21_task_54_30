package com.cpet;

public class CPet {
	/**
	 * 
	 */
	public CPet() {
		super();
	}

	/**
	 * @param age
	 * @param name
	 */
	public CPet(int age, String name) {
		super();
		this.age = age;
		this.name = name;
	}

	public int age;
	public String name;

	protected void bark() {
		System.out.println("barking...");
	}

	protected void print() {
		System.out.println("Pet name: " + name);
		System.out.println("Pet age: " + this.age);
	}

	protected void playing() {
		System.out.println("playing...");
	}
}
